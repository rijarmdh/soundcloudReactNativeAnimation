 import React from 'react';
import { StackNavigator, SwitchNavigator, createBottomTabNavigator  } from 'react-navigation';
import HomeScreen from './pages/home';
import LoginScreen from './pages/login';
import {StyleSheet, Text, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import StreamScreen from './pages/stream';
import SearchScreen from './pages/search';
import ProfileScreen from './pages/profile';
import Tabs from './bottomNav'

export default Switch = SwitchNavigator(
  {
    Login: LoginScreen,
    Home:Tabs
  },
  {
    initialRouteName:'Login'
  }  
);