import React from 'react';
import {StyleSheet, View, Button} from 'react-native';

export default class SearchScreen extends React.Component{
    render(){
        return(
            <View style={styles.container} >
                <Button
                    title = 'Im the SearchScreen'
                    onPress = {()=>this.props.navigation.navigate('Profile')}
                />
            </View>
        )
    }
}

const styles= StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    }
})