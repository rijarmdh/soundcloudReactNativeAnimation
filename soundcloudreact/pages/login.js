import React from 'react';
import {
    View, 
    StyleSheet, 
    Text, 
    Button, TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class LoginScreen extends React.Component{
    click = ()=>this.props.navigation.navigate('Home');

    render(){
        
        return(
            <View style={styles.container}>
                <Icon 
                    name='soundcloud' size={50} color={'white'}
                    onPress={this.click}
                />

                <TouchableHighlight            
                    onPress={ this.click }
                >
                <Text style={{color:'#fff', fontWeight: '100'}}>Go To  Home Page</Text>
                </TouchableHighlight>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ff8c00'
    }
});