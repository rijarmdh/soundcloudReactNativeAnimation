import React from 'react';
import {
    StyleSheet,
    Button, Text,
    View, TouchableHighlight
} from 'react-native';

export default class HomeScreen extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <TouchableHighlight
                    onPress = {()=>this.props.navigation.navigate('Stream')}
                >
                <Text style={{fontWeight: '100'}}>Im HomeScreen</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5fcff',
    }
})