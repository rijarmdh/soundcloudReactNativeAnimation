import { AppRegistry, YellowBox } from 'react-native';
import Switch from './App';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'])

AppRegistry.registerComponent('soundcloudreact', () => Switch);
