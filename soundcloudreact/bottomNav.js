import React from 'react';
import { StackNavigator, SwitchNavigator, createBottomTabNavigator  } from 'react-navigation';
import HomeScreen from './pages/home';
// import LoginScreen from './pages/login';
// import {StyleSheet, Text, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import StreamScreen from './pages/stream';
import SearchScreen from './pages/search';
import ProfileScreen from './pages/profile';

export default Tabs = createBottomTabNavigator(
    {
      Home: {screen:HomeScreen},
      Stream: {screen:StreamScreen},
      Search:{screen:SearchScreen},
      Profile: {screen:ProfileScreen}
    }, 
    
    {
      navigationOptions:({navigation})=>({
        tabBarIcon:({tintColor})=>{
          const {routeName} = navigation.state
          let iconName
          if(routeName === 'Home'){
            iconName = 'home'
          }else if(routeName === 'Stream'){
            iconName ='flash'
          }else if(routeName === 'Search'){
            iconName = 'search'
          }else if(routeName === 'Profile'){
            iconName = 'account'
          }
  
          return iconName === 'search' ? 
          <MaterialIcons name={iconName} size={25} color={tintColor }/>:
          <MaterialCommunityIcons name={iconName}  size={25} color={tintColor}/>
        }
      }),
      tabBarOptions:{
        activeTintColor:'#fff',
        inactiveTintColor:'grey',
        activeBackgroundColor:'grey',
        showLabel:false,
        style:{
          backgroundColor: '#000'
        }
      }
    }
  )
